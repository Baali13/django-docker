FROM python:3

WORKDIR /var/www/project

RUN pip install --upgrade pip
RUN pip install django
RUN pip install Pillow

ENTRYPOINT ["python", "manage.py"]
CMD ["runserver", "0.0.0.0:8000"]