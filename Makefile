ifneq (,$(wildcard ./.env))
    include .env
endif

DOCKER_COMP = docker compose

# DOCKER
build:
	@$(DOCKER_COMP) build --pull --no-cache

up:
	@$(DOCKER_COMP) up

stop:
	@$(DOCKER_COMP) stop	

down:
	@$(DOCKER_COMP) down --remove-orphans

restart: stop up

rebuild: down build up

bash:
	docker exec -it djadock bash